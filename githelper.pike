#! /usr/bin/env pike

#if 0	/* /bin/sh code below */
#! The following is debug code to simplify identifying the
#! pike binary that is used when running the helper.
#! It is enabled by prepending the top line of the file
#! with #!/bin/sh

type pike

echo "$""0: $0"
echo "$""@:" "$@"

exec /usr/bin/env pike "$0" "$@"
#endif	/* /bin/sh code above */

#if !constant(zero)
protected typedef int(0..0) zero;
#endif

// Enable GitLab issue integration with Pike.
#define COMMIT_PLUGIN "pike-gitlab.pike"

#define DOLLAR "$"
constant unexpanded_id = DOLLAR"Id"DOLLAR;

mapping(string:program) hooks = ([
  "pre-commit" : PreCommitHook,
  "pre-receive" : PreReceiveHook,
  "post-commit" : PostCommitHook,
  "post-rewrite" : PostRewriteHook,
  "githelper-plugins" : 0,
]);

mapping(string:program) filters = ([
#if 0
  "nice_ident" : NiceIdentFilter,
#endif
]);

string plugin_name = getenv("GITHELPER_PLUGIN")
#ifdef COMMIT_PLUGIN
  || COMMIT_PLUGIN
#endif
  ;

string git_binary = "git";

constant filterops = ({ "clean", "smudge" });

void fail(string msg, mixed ... args)
{
  werror(msg, @args);
  exit(1);
}

void iofailn(int errno, string msg, mixed ... args)
{
  fail(msg+": %s\n", @args, strerror(errno));
}

void iofail(string msg, mixed ... args)
{
  iofailn(errno(), msg, @args);
}

array(string) split_z(string data)
{
  array(string) a = data / "\0";
  if (sizeof(a) && a[-1] == "")
    a = a[..sizeof(a)-2];
  return a;
}

array(string) split_lf(string data)
{
  array(string) a = data / "\n";
  if (sizeof(a) && a[-1] == "")
    a = a[..sizeof(a)-2];
  return a;
}

string run_git_ex(int max_exitcode, string ... args)
{
  object(Stdio.File)|zero mystdout = Stdio.File();
  object(Stdio.File)|zero mystderr = Stdio.File();
  Process.Process p = Process.Process (({ git_binary }) + args, ([
					 "stdout":mystdout->pipe(),
					 "stderr":mystderr->pipe(),
				       ]));
  string gotstdout="", gotstderr="";
  mystdout->set_read_callback( lambda( mixed i, string data) {
				 gotstdout += data;
			       } );
  mystderr->set_read_callback( lambda( mixed i, string data) {
				 gotstderr += data;
			       } );
  mystdout->set_close_callback( lambda () {
				  mystdout->set_read_callback(0);
				  mystdout = 0;
				});
  mystderr->set_close_callback( lambda () {
				  mystderr->set_read_callback(0);
				  mystderr = 0;
				});
  while( mystdout || mystderr )
    Pike.DefaultBackend( 1.0 );

  int exitcode = p->wait();

  if (exitcode > max_exitcode) {
    werror(gotstderr);
    fail("git %{%O %}exited with code %d\n", args, exitcode);
  }
  if (exitcode) return "";
  return gotstdout;
}

string run_git(string ... args)
{
  return run_git_ex(0, @args);
}

string get_staged_file(string filename, int|void allow_empty)
{
  string blob;
  string treeentry = run_git("ls-files", "--stage", "--", filename);
  if (allow_empty && !sizeof(treeentry))
    return "";
  if (2 != sscanf(treeentry, "%*o %s ", blob))
    fail("Unable to parse output from git ls-files...\n");
  return run_git("cat-file", "blob", blob);
}

string get_committed_file(string sha, string filename, int|void allow_empty)
{
  string blob;
  string treeentry = run_git("ls-tree", sha, "--", filename);
  if (allow_empty && !sizeof(treeentry))
    return "";
  if (allow_empty == 2 && 2 == sscanf(treeentry, "%*o tree %s\t", blob))
    return "";
  if (2 != sscanf(treeentry, "%*o blob %s\t", blob))
    fail("Unexpected output from git ls-tree\n");
  return run_git("cat-file", "blob", blob);
}

string check_commit_timestamps(string commit)
{
  int cct, cat, pct, pat;
  string parents;
  int sysclock = time() + 60;
  if(3 != sscanf(run_git("log", "-n", "1", "--format=%ct %at %P", commit),
		 "%d %d %s\n", cct, cat, parents))
    fail("Unexpected output from git log");
  if (cat > sysclock)
    return "Author time is in the future";
  if (cct > sysclock)
    return "Commit time is in the future";
  if (cat > cct)
    return "Author time is later than commit time";
  if (sizeof(parents))
    foreach(parents/" ", string parent) {
      if(2 != sscanf(run_git("log", "-n", "1", "--format=%ct %at", parent),
		     "%d %d", pct, pat))
	fail("Unexpected output from git log");
      if (cct < pct)
	return "Commit time is before that of parent "+parent;
    }
  return 0;
}

int is_encoding_utf8(string name)
{
  return (!name) || (<"utf-8", "utf8">)[lower_case(name)];
}

string check_encoding(string data, string|void encoding)
{
  if(is_encoding_utf8(encoding))
    encoding = "UTF-8";
  mixed err = catch {
#if constant(Charset)
      // Pike 8.0 and later.
      Charset.Decoder decoder = Charset.decoder(encoding);
#elif constant(Locale.Charset.Decoder)
      // Pike 7.8.
      Locale.Charset.Decoder decoder = Locale.Charset.decoder(encoding);
#else
      // Pike 7.4
      object decoder = Locale.Charset.decoder(encoding);
#endif
      foreach(Array.uniq(values(decoder->feed(data)->drain())), int c)
	switch(c) {
	case 0xfffd:
	  return "Undefinied character detected\n";
	case '\t':
	case '\n':
	case '\r': /* ? */
	  /* Allowed control character */
	  break;
	default:
	  if (c<32 || (c>=0x80 && c<0xa0))
	    return sprintf("Forbidden control character 0x%02x detected\n", c);
	}
    };
  return err && err[0];
}

class GitAttributes
{
  enum {
    ATTR_TRUE = 1,
    ATTR_FALSE = 2,
    ATTR_UNSET = 3
  };

  class AttrState(string attr, string|int setto) {
    protected string _sprintf(int type) {
      return type=='O' && sprintf("AttrState(%O, %O)\n", attr, setto);
    }
  };

  class MatchAttr(string name, int is_macro, array(AttrState) states) {
    protected string _sprintf(int type) {
      return type=='O' && sprintf("MatchAttr(%O, %d, %O)\n",
				  name, is_macro, states);
    }
  };

  protected array(MatchAttr) attrs = ({});
  protected mapping(string:MatchAttr) macros = ([]);

  protected int invalid_attr_name(string name)
  {
    int n;
    if(name == "" || name[0] == '-')
      return 1;
    sscanf(name, "%*[-._0-9a-zA-Z]%n", n);
    return n != sizeof(name);
  }

  protected AttrState parse_attr(string src)
  {
    string|zero equals = 0;
    string|int setto;
    sscanf(src, "%s=%s", src, equals);
    if(src[0] == '-' || src[0] == '!') {
      setto = (src[0]=='-'? ATTR_FALSE : ATTR_UNSET);
      src = src[1..];
    } else
      setto = equals || ATTR_TRUE;
    if(invalid_attr_name(src))
      fail("%s is not a valid attribute name\n", src);
    return AttrState(src, setto);
  }

  protected MatchAttr parse_attr_line(string line, int macro_ok)
  {
    int is_macro=0;
    string name;
    line = String.trim_whites(replace(line, ({"\t","\r","\n"}),
				      ({" ", " ", " "})));
    if(!sizeof(line) || line[0] == '#')
      return 0;
    if(has_prefix(line, "[attr]")) {
      if(!macro_ok)
        fail("%s not allowed\n", line);
      is_macro=1;
      sscanf(line, "[attr]%*[ ]%s%*[ ]%s", name, line);
    } else {
      // FIXME: Lines starting with a double-quote use C-style
      //        string-escapes for the file name.
      sscanf(line, "%s%*[ ]%s", name, line);
    }
    return MatchAttr(name, is_macro, reverse(map(line/" "-({""}), parse_attr)));
  }

  protected void handle_attr_line(string line, int macro_ok)
  {
    MatchAttr a = parse_attr_line(line, macro_ok);
    if(a) attrs += ({ a });
  }

  protected void create(string data)
  {
    foreach(data/"\n", string line)
      handle_attr_line(line, 1);
    attrs = reverse(attrs);
    foreach(attrs, MatchAttr a)
      if(a->is_macro)
	macros[a->name] = a;
    attrs = filter(attrs, lambda(MatchAttr a) { return !a->is_macro; });
  }

  protected int path_matches(string path, string pattern)
  {
    if(search(pattern, "/")<0)
      return glob(pattern, (path/"/")[-1]);
    if(has_prefix(pattern, "/"))
      pattern = pattern[1..];
    return glob(pattern, path);
  }

  protected void macroexpand_one(string attrname, array(MatchAttr) attrs,
				 mapping(string:string|int) all_attr)
  {
    if(all_attr[attrname] != ATTR_TRUE)
      return;
    MatchAttr ma = macros[attrname];
    if(ma) fill_one(ma, attrs, all_attr);
  }

  protected void fill_one(MatchAttr attr, array(MatchAttr) attrs,
			  mapping(string:string|int) all_attr)
  {
    foreach(attr->states, AttrState s) {
      if(!all_attr[s->attr]) {
	all_attr[s->attr] = s->setto;
	macroexpand_one(s->attr, attrs, all_attr);
      }
    }
  }

  protected void fill(string path, array(MatchAttr) attrs,
		      mapping(string:string|int) all_attr)
  {
    foreach(attrs, MatchAttr a)
      if(/*!a->is_macro &&*/ path_matches(path, a->name))
	fill_one(a, attrs, all_attr);
  }

  mapping(string:string|int) checkattr(string path)
  {
    mapping(string:string|int) all_attr = ([]);
    fill(path, attrs, all_attr);
    return all_attr;
  }

  array(string) findattr(string attrname)
  {
    array(string) r = ({});
    foreach(attrs+values(macros), MatchAttr attr) {
      int z=0;
      foreach(attr->states, AttrState state)
	if(state->attr == attrname && state->setto == ATTR_TRUE) {
	  z = 1;
	  break;
	}
      if (z)
	r += ({ attr->name });
    }
    return r;
  }

  protected string _sprintf(int type) {
    return type=='O' && sprintf("GitAttributes(%O)\n", attrs);
  }
}


private string git_dir;
private string git_commondir;

string get_git_dir()
{
  return git_dir ||
    (git_dir = String.trim_all_whites(run_git("rev-parse", "--git-dir")));
}

string get_git_commondir()
{
  if (git_commondir) return git_commondir;

  string dir = get_git_dir();
  string commondir = Stdio.read_bytes(combine_path(dir, "commondir"));
  if (!commondir) {
    // Not a worktree.
    return git_commondir = dir;
  }
  // We are in a worktree.
  // NB: commondir may be relative to gitdir. It may also have
  //     a terminating linefeed.
  return git_commondir = combine_path(dir, String.trim_all_whites(commondir));
}


/* Hooks */

class CommitHookUtils
{
  protected array(string) files_to_commit;
  GitAttributes attrs;

  class GithelperPlugin
  {
    optional string check_message_body(string sha, array(string) headers,
				       string body);

    optional int(0..1) check_commit(string branch, string sha,
                                    array(string) paths,
                                    int|void just_check);
  }

  GithelperPlugin plugin;

  private class PluginHandler
  {
    mixed resolv(string symbol, string file, mixed ... rest)
    {
      switch(symbol) {
      case "GithelperPlugin": return GithelperPlugin;
      case "get_git_dir": return get_git_dir;
      case "get_git_commondir": return get_git_commondir;
      case "run_git": return run_git;
      case "run_git_ex": return run_git_ex;
      }
      return master()->resolv(symbol, file);
    }
  }

  private PluginHandler plugin_handler = PluginHandler();

  protected void create()
  {
    if (!sizeof(plugin_name || "")) return;

    string plugin_path = combine_path(get_git_commondir(),
				      "hooks/githelper-plugins",
				      plugin_name);
    if (Stdio.exist(plugin_path)) {
      program(GithelperPlugin) plugin_program =
	compile_file(plugin_path, plugin_handler);
      plugin = plugin_program();
    } else {
      write("NOTICE: Plugin %s not found.\n", plugin_path);
      write("You may want to reinstall githelper.\n");
    }
  }

  string get_file(string filename, int|void allow_empty);
  string get_old_file(string filename, int|void allow_empty);
  int entry_is_new(string filename) { return 0; }

  int find_expanded_ident(string data)
  {
    int p=0;
    while ((p = search(data, DOLLAR"Id", p))>=0) {
      if (data[p..p+3] != unexpanded_id) {
	int p2 = search(data, DOLLAR, p+3), p3 = search(data, "\n", p+3);
	if (p2 > p && (p3 < 0 || p2 < p3))
	  return 1;
      }
      p += 4;
    }
    return 0;
  }
  
  int check_ident(string filename)
  {
    if (find_expanded_ident(get_file(filename, 2))) {
      write("File %s contains an expanded ident.\n", filename);
      if(this_program == PreCommitHook) {
	write("Try 'git reset %s; git add %s', "
	      "or remove the ident manually.\n",
	      @({filename})*2);;
      }
      return 1;
    }
    return 0;
  }

  int check_blocker_attributes()
  {
    int err = 0;
    foreach(files_to_commit, string filename) {
      mapping(string:string|int) a = attrs->checkattr(filename);
      if(a->foreign_ident == GitAttributes.ATTR_TRUE) {
	if (!entry_is_new(filename)) {
	  write("File %s has the foreign_ident attribute.  Please remove it before commit.\n", filename);
	  err = 1;
	  continue;
	}
      }
      if(stringp(a->block_commit) || a->block_commit == GitAttributes.ATTR_TRUE) {
	if (!entry_is_new(filename)) {
	  write("File %s is blocked from committing: %s\n", filename,
		replace((stringp(a->block_commit)? a->block_commit :
			 "no explanation given"), "-", " "));
	  err = 1;
	  continue;
	}
      }
      if(a->ident && a->ident != GitAttributes.ATTR_FALSE &&
	 a->ident != GitAttributes.ATTR_UNSET) {
	if (check_ident(filename)) {
	  err = 1;
	  continue;
	}
      }
      if (a->binary != GitAttributes.ATTR_TRUE) {
	string illegal_indent;
        string legal_indent;
	if (a->space_indent == GitAttributes.ATTR_TRUE) {
	  // Canonical indent is spaces only.
	  illegal_indent = "\n\t";
	} else if (a->tab_indent == GitAttributes.ATTR_TRUE) {
	  // Canonical indent is initial tab.
	  illegal_indent = "\n        ";
          // Allow indent with spaces if they belong to the
          // previous line.
          legal_indent = "\\" + illegal_indent;
	}
	if (illegal_indent) {
	  if (a->mixed_indent == GitAttributes.ATTR_TRUE) {
            string data = "\n" + get_file(filename, 2);
            int count = sizeof(data/illegal_indent) -
              (legal_indent?sizeof(data/legal_indent):1);
	    if (!count) {
	      write("File %s has the mixed_indent attribute.  Please remove it before commit.\n", filename);
	      err = 1;
	      continue;
	    }
            data = get_old_file(filename, 2);
            int old_count = sizeof(data/illegal_indent) -
              (legal_indent?sizeof(data/legal_indent):1);
	    if (old_count < count) {
	      if (illegal_indent == "\n\t") {
		write("File %s adds line indented with tab.\n", filename);
	      } else {
		write("File %s adds line indented with spaces.\n", filename);
	      }
	      err = 1;
	      continue;
	    }
	  } else {
            string data = "\n" + get_file(filename, 2);
	    if (has_value(data, illegal_indent)) {
              if (legal_indent) {
                // We need to check if they are all legal.
                int count = sizeof(data/illegal_indent) -
                  sizeof(data/legal_indent);
                if (!count) continue;
              }
	      if (illegal_indent == "\n\t") {
		write("File %s has a line indented with tab.\n", filename);
	      } else {
		write("File %s has a line indented with spaces.\n", filename);
	      }
	      err = 1;
	      continue;
	    }
	  }
	}
      }
    }
    return err;
  }

  int check_gitattributes_files()
  {
    int err = 0;
#if 0
    foreach(files_to_commit, string filename)
      if(has_suffix(filename, "/.gitattributes")) {
	write(".gitattributes are not allowed in subdirectories; "
	      "please remove %s\n", filename);
	err = 1;
      }
#endif

    if(search(files_to_commit, ".gitattributes")>=0) {
      GitAttributes old_attrs =
	GitAttributes(get_old_file(".gitattributes", 1));

      array(string) new_f_i = sort(attrs->findattr("foreign_ident"));
      array(string) old_f_i = sort(old_attrs->findattr("foreign_ident"));
      array(string) added_fi = new_f_i - old_f_i;
      array(string) removed_fi = old_f_i - new_f_i;

      foreach(added_fi, string path) {
	if(!has_prefix(path, "/") || search(path, "*")>=0) {
	  write("Commit adds unsupported foreign_ident: %s\n", path);
	  err = 1;
	  continue;
	}
	path = path[1..];
	if (!entry_is_new(path)) {
	  write("Commit adds foreign_ident to existing file %s\n", path);
	  err = 1;
	}
      }

      foreach(removed_fi, string path) {
	if(has_prefix(path, "/"))
	  path = path[1..];
	if (search(files_to_commit, path)<0 &&
	    find_expanded_ident(get_file(path, 2))) {
	  write("Commit removes foreign_ident from unchanged file %s\n", path);
	  err = 1;
	}
      }
    }
    return err;
  }
}

class CommitHookUtilsRepo
{
  inherit CommitHookUtils;

  protected string sha;
  protected string parent;

  protected mapping(string:array(string)) commit_paths = ([]);

  string get_file(string filename, int|void allow_empty)
  {
    return get_committed_file(sha, filename, allow_empty);
  }

  string get_old_file(string filename, int|void allow_empty)
  {
    return get_committed_file(parent, filename, allow_empty);
  }

  int entry_is_new(string filename)
  {
    return (!sizeof(run_git("ls-tree", parent, "--", filename))) &&
      sizeof(run_git("ls-tree", sha, "--", filename));
  }

  string check_commit_msg(string commit)
  {
    string message = run_git("cat-file", "commit", commit);
    string|zero encoding = 0;
    string headers = (message/"\n\n")[0];
    string body = (message/"\n\n")[1..]*"\n\n";
    foreach(headers/"\n", string headerline) {
      if(has_prefix(headerline, "encoding "))
	encoding = headerline[9..];
    }

    string msg = check_encoding(message, encoding);

    if (msg) return msg;

    if (plugin && plugin->check_message_body) {
      return plugin->check_message_body(commit, headers/"\n", body);
    }

    return 0;
  }

  int check_commit(string sha)
  {
    if (!has_prefix(sha, "HEAD"))
      write("Checking commit %s\n", sha);
    this_program::sha = sha;
    array(string) parents = split_lf(run_git("rev-list", "--parents",
					     "-n", "1", sha))[0] / " ";
    if (sizeof(parents) > 1)
      parent = parents[1]; // First parent, sha^
    else
      parent = String.trim_all_whites(run_git("hash-object", "-t", "tree", "/dev/null")); // Empty tree
    commit_paths[sha] = files_to_commit =
      split_z(run_git("diff", "--name-only", "-z", sha, parent));
    attrs = GitAttributes(get_file(".gitattributes", 1));
    string ts_test = check_commit_timestamps(sha);
    int err = 0;
    if (ts_test) {
      write("Invalid timestamps: %s\n", ts_test);
      err = 1;
    }
    string cm_test = check_commit_msg(sha);
    if (cm_test) {
      write("NOTICE: Your commit message has an encoding problem:\n%s", cm_test);
      write("Please amend it before pushing.\n");
      err = 1;
    }
    string branch = String.trim_all_whites(run_git("branch", "--show-current"));
    if (plugin && plugin->check_commit("refs/heads/" + branch, sha,
                                       files_to_commit, 1)) {
      err = 1;
    }
    return check_blocker_attributes() |
      check_gitattributes_files() |
      err;
  }
}

/* Checks run before editing a commit message */

class PreCommitHook
{
  inherit CommitHookUtils;

  string get_file(string filename, int|void allow_empty)
  {
    return get_staged_file(filename, allow_empty);
  }

  string get_old_file(string filename, int|void allow_empty)
  {
    return get_committed_file("HEAD", filename, allow_empty);
  }

  int check_attributes_staged()
  {
    // We don't allow .gitattributes to differ between wt and index,
    // because that could mean the committed stuff ends up with different
    // attributes than they have right now...

    if (sizeof(run_git("diff", "--name-only", "--", ".gitattributes"))) {
      write("You have unstaged changes to .gitattributes.\n"
	    "Please add or stash them before commit.\n");
      return 1;
    }
  }

  int check_not_tracking()
  {
    string local_branch = run_git_ex(128, "symbolic-ref", "HEAD");
    if (local_branch == "") {
      return 0;
    }
    if (!has_prefix(local_branch, "refs/heads/")) {
      write("Attempt to commit to non-branch: %s.\n", local_branch);
    }
    if (has_prefix(local_branch, "refs/heads/tracking/")) {
      write("Attempt to commit to tracking branch.\n");
      return 1;
    }
    local_branch = local_branch[sizeof("refs/heads/")..];
    if (has_suffix(local_branch, "\n")) {
      local_branch = local_branch[..<1];
    }
    string remote_branch = run_git_ex(1, "config", "--get",
				      "branch." + local_branch + ".merge");
    if (has_prefix(remote_branch, "refs/heads/tracking/")) {
      write("Attempt to commit to remote tracking branch.\n");
      return 1;
    }
  }

  int hook()
  {
    files_to_commit =
      split_z(run_git("diff", "--staged", "--name-only", "-z"));
    attrs = GitAttributes(get_file(".gitattributes", 1));
    return
      check_not_tracking() |
      check_attributes_staged() |
      check_blocker_attributes() |
      check_gitattributes_files();
  }
}

/* Checks run before accepting a push */

class PreReceiveHook
{
  inherit CommitHookUtilsRepo;

  enum AccessLevel {
    ACCESS_NONE = 0,
    ACCESS_BASIC = 1,
    ACCESS_GROUP = 2,	// Create group tags.
    ACCESS_EXISTING = 3, // May be updated to any previously existing commit.
    ACCESS_FULL = 4,	// rebase/delete branch, move/delete tag, etc.
  };

  protected mapping(string:AccessLevel) groups = ([
    "scratch": ACCESS_FULL,
  ]);

  protected array(string) commits_to_check = ({});

  protected mapping(string:array(string)) branch_commits = ([]);

  protected void parse_groups(string user)
  {
    string git_commondir = get_git_commondir();

    groups[user] = ACCESS_FULL;
    groups["tracking"] = ACCESS_EXISTING;

    string group_data =
      Stdio.read_bytes(combine_path(git_commondir, "info/group"));

    if (!group_data) return;

    foreach(replace(group_data, "\r", "\n")/"\n", string line) {
      array(string) fields = map(line/":", String.trim_all_whites);

      // NB: We currently only care about fields 0 (group name)
      //     and 3 (member list). The first member of a group is
      //     considered the primary member, and has full access.

      if ((sizeof(fields) != 4) || (fields[0] == "")) continue;
      array(string) members = map(fields[3]/",", String.trim_all_whites);
      if (members[0] == user) {
	// Primary member.
	groups[fields[0]] = ACCESS_FULL;
	continue;
      }
      if (groups[fields[0]]) continue;	// Already a member.
      if (has_value(members, user)) {
	groups[fields[0]] = ACCESS_GROUP;
	continue;
      }
      groups[fields[0]] = ACCESS_NONE;
    }

    // Restore master permission in case username == groupname.
    groups[user] = ACCESS_FULL;
  }

  AccessLevel check_access(string ref_name, string user)
  {
    /* Return ACCESS_NONE (0) for no access,
     *        ACCESS_BASIC (1) for basic access, and
     *        ACCESS_FULL (4) for full access
     *        (including rebase/delete branch, and move/delete tag)
     */
    string shortref = ref_name;
    sscanf(shortref, "refs/%*[^/]/%s", shortref);
    foreach(groups; string group; AccessLevel ac) {
      if (has_prefix(shortref, group + "/")) {
	if (!ac) {
	  write("Access to %s denied for user %s\n", ref_name, user);
	}
	return ac;
      }
    }
    if (search(ref_name, "/x-") >= 0) {
      write("The ref %s can only be modified by its owner\n", ref_name);
      return ACCESS_NONE;
    }
    return ACCESS_BASIC;
  }

  int check_tag_push(string old_sha, string new_sha, string ref_name,
		     AccessLevel access_level)
  {
    if (access_level >= ACCESS_FULL)
      return 0;

    string oldtag =
      String.trim_all_whites(run_git_ex(1, "rev-parse", "--verify",
					"-q", ref_name));
    if (sizeof(oldtag) && oldtag != new_sha) {
      write("Tag %s already exists with value %s, will not %s it\n",
	    ref_name, oldtag, (new_sha == "0"*40? "delete":"move"));
      return 1;
    }

    if (!sizeof(oldtag) && search(ref_name[10..], "/") >= 0) {
      if (access_level >= ACCESS_GROUP) {
	// Group tag and we have permission.
	return 0;
      }
      write("Common tags are not allowed to contain /.\n");
      return 1;
    }

    return 0;
  }

  int check_branch_push(string old_sha, string new_sha, string ref_name,
			AccessLevel access_level)
  {
    if (old_sha == "0"*40) {
      // New branch, check if the name is allowed...
      if (sscanf(ref_name, "refs/heads/%*[0-9.]%*c") < 2) {
	write("Main version branches can not be created remotely.\n");
	return 1;
      }
      if (access_level < ACCESS_GROUP && search(ref_name[11..], "/")>=0) {
	write("Common topic branch names are not allowed to contain /.\n");
	return 1;
      }
      return 0;
    } else if (new_sha == "0"*40) {
      // Delete old branch
      if (access_level < ACCESS_EXISTING) {
	write("You may not delete branches which do not belong to you.\n");
	return 1;
      }
      return 0;
    } else {
      // FIXME: Ought to ensure that the commit allready exists
      //        for ACCESS_EXISTING.
      if (access_level >= ACCESS_EXISTING)
	/* Skip checks */
	return 0;

      string merge_base =
	String.trim_all_whites(run_git("merge-base", old_sha, new_sha));
      if (merge_base != old_sha) {
	write("Push to %s is not fast-forward.\n", ref_name);
	return 1;
      }
      array(string) old_depth = split_lf(run_git("rev-list", "--first-parent",
						 "-n", "2", old_sha));
      array(string) fp_path = split_lf(run_git("rev-list", "--first-parent",
					       (sizeof(old_depth)<2?
						new_sha :
						old_sha+"^.."+new_sha)));
      if (search(fp_path, old_sha)<0) {
	write("Commit %s does not contain %s in its first-parent ancestry.\nDid you pull with merge instead of rebase?\n", new_sha, old_sha);
	return 1;
      }

      array(string) commits =
	reverse(split_lf(run_git("rev-list", old_sha+".."+new_sha)));

      branch_commits[ref_name] += commits;
      commits_to_check += commits;
      return 0;
    }
  }

  int check_push(string git_user,
		 string old_sha, string new_sha, string ref_name)
  {
    AccessLevel access_level;
    if(!(access_level = check_access(ref_name, git_user))) return 1;
    if (has_prefix(ref_name, "refs/tags/")) {
      return check_tag_push(old_sha, new_sha, ref_name, access_level);
    } else if (has_prefix(ref_name, "refs/heads/")) {
      return check_branch_push(old_sha, new_sha, ref_name, access_level);
    } else {
      write("Trying to push a ref which is neither under refs/tags/ or refs/heads/...\n");
      return 1;
    }
  }

  int hook()
  {
    string git_user = getenv("GIT_USER")||getenv("USER")||"nobody";
    parse_groups(git_user);
    foreach(split_lf(Stdio.stdin->read()), string line) {
      array(string) args = line / " ";
      if(sizeof(args) != 3)
	fail("Unexpected input line to pre-receive hook: %s\n", line);
      if(check_push(git_user, @args))
	return 1;
    }
    foreach(Array.uniq(commits_to_check), string sha)
      if(check_commit(sha))
	return 1;

    if (plugin && plugin->check_commit) {
      foreach(sort(indices(branch_commits)), string branch) {
	foreach(branch_commits[branch], string sha) {
	  if (plugin->check_commit(branch, sha, commit_paths[sha])) {
	    return 1;
	  }
	}
      }
    }

    return 0;
  }
}

/* Do housekeeping after a commit */

class PostCommitHook
{
  inherit CommitHookUtilsRepo;

  void cleanup(string filename, mapping(string:string|int) attr)
  {
    if(attr->ident && attr->ident != GitAttributes.ATTR_FALSE &&
       attr->ident != GitAttributes.ATTR_UNSET &&
       search(get_committed_file("HEAD", filename, 1), unexpanded_id)>=0)
      if(sizeof(run_git("diff", "--name-only", "--", filename))) {
	write("NOTICE: The file %s has a stale ident,\n        but I won't touch it since you have unstaged changes.\n", filename);
      } else {
	// write("Checking out %s, to fix stale ident...\n", filename);
	run_git("checkout", "HEAD", "--", filename);
      }
  }

  int hook()
  {
    if (check_commit("HEAD"))
      write("NOTICE: Your commit has errors, see above messages.  Please amend before push.\n");
    foreach(files_to_commit, string filename)
      cleanup(filename, attrs->checkattr(filename));
    string ts_test = check_commit_timestamps("HEAD");
    if (ts_test) {
      write("NOTICE: Your commit has invalid timestamps: %s\n", ts_test);
      write("Please amend it before pushing.\n");
    }
    return 0;
  }
}

class PostRewriteHook
{
  inherit CommitHookUtilsRepo;

  int hook(string command)
  {
    if (command == "rebase") {
      int errs = 0;
      foreach(split_lf(Stdio.stdin->read()), string line) {
	string old_sha, new_sha, extra;
	if(sscanf(line, "%s %s%*[ ]%s", old_sha, new_sha, extra) != 4)
	  fail("Unparsable input line %O!\n", line);
	errs += check_commit(new_sha);
      }
      if (errs)
	write("NOTICE: %d of the commits contain errors.  Please amend before pushing.\n", errs);
    }
    return 0;
  }
}

/* Filters */

/* A sample filter, not really useful... */

class NiceIdentFilter
{
  protected string replace_id(string f, function(string:string) replace) {
    int p=0;
    while((p=search(f, DOLLAR"Id", p)) >= 0) {
      int p2 = search(f, DOLLAR, p+3), p3 = search(f, "\n", p+3);
      if (p2 > p && (p3 < 0 || p2 < p3)) {
	string r = replace(f[p..p2]);
	if (r) {
	  // werror("Replacing %O with %O\n", f[p..p2], r);
	  f = f[..p-1]+r+f[p2+1..];
	  p += sizeof(r);
	} else {
	  // werror("Not replacing %O\n", f[p..p2]);
	  p = p2+1;
	}
      } else p += 3;
    }
    return f;
  }

  protected string clean_ident(string i)
  {
    if(has_prefix(i, DOLLAR"Id:") && sizeof(i/" ")==13)
      return unexpanded_id;
  }
 
  protected string smudge_ident(string i)
  {
    return DOLLAR"Id: some nice ident perhaps, but based on what? "DOLLAR;
  }

  int clean()
  {
    write(replace_id(Stdio.stdin->read(), clean_ident));
    return 0;
  }

  int smudge()
  {
    write(replace_id(Stdio.stdin->read(), smudge_ident));
    return 0;
  }
}

/* Main helper */

class GitHelper
{
  void setup_hooks()
  {
    if (!sizeof(hooks))
      return;
    string hooksdir = combine_path(get_git_commondir(), "hooks");
    if (!file_stat(hooksdir)) {
      write("Creating the hooks directory\n");
      if (!mkdir(hooksdir))
	iofail("Failed to create %s", hooksdir);
    }
    foreach (hooks; string name; program p) {
      string path = combine_path(hooksdir, name);
      string src = p?__FILE__:combine_path(__DIR__, name);
      if (!file_stat(src)) {
	iofail("Source file %s not found");
      }
      Stdio.Stat s = file_stat(path, 1);
      if (!s) {
	write("Installing %s\n", path);
	System.symlink(src, path);
      } else if (s->islnk) {
	/* Already setup ok, it seems */
      } else {
	write("Hook %s already exists, so won't overwrite it...\n", name);
      }
    }
  }

  void setup_filter(string name, string op)
  {
    string confname = "filter."+name+"."+op;
    string old = String.trim_all_whites(run_git_ex(1, "config", "--get", confname));
    string cmd = __FILE__+" filter_"+name+"_"+op;
    if (old == "") {
      write("Installing filter operation %s\n", confname);
      run_git("config", confname, cmd);
    } else if(old == cmd) {
      /* Already has correct value */
    } else {
      write("Filter operation %s is already set to %s, not modifying\n",
	    confname, old);
    }
  }

  void setup_filters()
  {
    foreach (filters; string name; program fprog) {
      object filter = fprog();
      foreach (filterops; ; string op)
	if (filter[op])
	  setup_filter(name, op);
    }
  }

  int setup(array(string) args)
  {
    if (sizeof(args)) {
      werror("githelper.pike should be invoked without arguments...\n");
      return 1;
    }
    if (!cd(String.trim_all_whites(run_git("rev-parse", "--git-dir"))))
      iofail("Failed to cd to .git directory");
    setup_hooks();
    setup_filters();
    return 0;
  }
}

string get_filter_op(string arg)
{
  if (!has_prefix(arg, "filter_"))
    return 0;
  foreach (filterops; ; string op)
    if (has_suffix(arg, "_"+op))
      return op;
  return 0;
}

int main(int argc, array(string) argv)
{
  if (!Process.search_path(git_binary)) {
    // Git binary not in $PATH. We might be running in a gitlab
    // docker image, in which case it is hidden in /opt/gitlab/embedded/bin/.
    foreach(({
              "/opt/gitlab/embedded/bin/git",
            }), string git_bin) {
      if (Stdio.exist(git_bin)) {
        git_binary = git_bin;
        break;
      }
    }
  }

  string command_name = basename(argv[0]);
  if (hooks[command_name]) {
    // write("Running hook %O:%O\n", command_name, hooks[command_name]);
    return hooks[command_name]()->hook(@argv[1..]);
  } else if (command_name == "githelper.pike") {
    string fop;
    if (argc>1 && (fop = get_filter_op(argv[1]))) {
      string filter = argv[1][7..sizeof(argv[1])-(sizeof(fop)+2)];
      if (filters[filter]) {
	object f = filters[filter]();
	if (!f[fop]) {
	  werror("Filter %s does not implement %s!\n", filter, fop);
	  return 1;
	} else
	  return f[fop](@argv[2..]);
      } else {
	werror("Unknown filter %s!\n", filter);
	return 1;
      }
    } else
      return GitHelper()->setup(argv[1..]);
  } else {
    werror("Unknown invocation method %s!\n", command_name);
    return 1;
  }
}
