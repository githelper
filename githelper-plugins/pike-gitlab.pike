//
// Issue-workflow for Pike.
//

inherit "gitlab.pike";

// GitLab defaults for read-only @git.lysator.liu.se/pikelang/pike/.
// This is the default gitlab if an access token is provided.
protected constant default_gitlab_url = "https://git.lysator.liu.se";
// Read-only gitlab relay:
protected constant default_gitlab_ro_url = "http://pike-librarian.lysator.liu.se";
protected constant default_gitlab_project_id = 636;

protected string update_issue_state(string sha,
				    mapping(string:mixed) issue,
				    array(string) paths,
				    multiset(string) event,
				    string branch)
{
#if 0
  write("update_issue_state(%O, %O, %O, %O, %O)\n",
        sha, issue, paths, event, branch);
#endif

  // Determine the controlling master branch for the issue
  // from what milestone it has.
  string master = "refs/heads/master";
  if (issue->milestone && has_prefix(issue->milestone->title || "", "Pike ")) {
    if (issue->milestone->title != "Pike Next") {
      string tentative_master = "refs/heads/" +
	issue->milestone->title[sizeof("Pike ")..];
      // Only select the new branch if it exists.
      // This can happen when the milestone has not
      // yet been splitted from the master branch.
      if (sizeof(run_git_ex(128, "rev-parse", tentative_master))) {
	master = tentative_master;
      }
    }
  }

  // Check if there is already a resolution marker.
  int resolvedp = sizeof(filter(issue->labels, has_prefix, "Resolution::"));

  // We're doing something so the issue is by definition not stalled.
  sub_label(issue, "Stalled");

  // Check if we're committing a fix on the master branch.
  if (branch == master) {
    if (event->fixed) {

      // We're resolving the issue (at least partially),
      // so remove the Doing marker.
      sub_label(issue, "Doing");

      if (!resolvedp) {
        // No previous resolution, so set the resolution to Fixed.
        add_label(issue, "Resolution::Fixed");
        resolvedp = 1;
      }
    }

    if (has_value(paths, "CHANGES")) {
      // We're updating the CHANGES file, so remove the
      // To Do: Changelog marker.
      sub_label(issue, "To Do: Change Log");
    }
  }

  if ((branch == master) || (branch == "refs/heads/master")) {
    // Some to do's may be relevant only on the true master branch,
    // so allow completion of them there too.

    if (sizeof(glob("*/testsuite.in", paths))) {
      // We're updating the testsuite, so remove the
      // To Do: Testsuite marker.
      sub_label(issue, "To Do: Testsuite");
    }

    if (event->tests) {
      sub_label(issue, "To Do: Testing");
    }

    if (sizeof(glob("refdoc/chapters/*", paths))) {
      // We're updating the manual, so remove the
      // To Do: Document marker.
      sub_label(issue, "To Do: Document");
    }

    if (event->documents) {
      sub_label(issue, "To Do: Document");
    }
  }

  if (resolvedp) {
    array(string) remaining_to_do =
      filter(issue->labels, has_prefix, "To Do:");
    if (!sizeof(remaining_to_do)) {
      // We have fixed the issue and there are no To Do's left,
      // so we can close the issue.
      close_issue(issue);
    }
  } else {
    // We're not done, but we're apparently doing something.
    // Set the Doing marker.
    add_label(issue, "Doing");
  }
}

// GitLab API
protected void report_issue_state(int issue_id, int|void just_check)
{
  mapping issue = get_issue(issue_id);
  if (!issue) return;

  array(string) remaining_to_do = filter(issue->labels, has_prefix, "To Do:");
  if (sizeof(remaining_to_do)) {
    write("  Remaining to do:\n");
    foreach(sort(remaining_to_do), string label) {
      write("    %s\n",
            String.trim_all_whites(label[sizeof("To Do:")..]));
    }
  }
}

// API
void check_message_body(string sha, array(string) headers, string body)
{
  ::check_message_body(sha, headers, body);

  array(string) a = body/"PIKE-";
  for (int i = 1; i < sizeof(a); i++) {
    int issue_id;
    if (sscanf(a[i], "%d", issue_id) && (issue_id > 0)) {
      // Old YouTrack PIKE-XXXX reference.
      // Remap to the migrated GitLab range.
      issue_id += 8000;

      array(string) b = ((lower_case(a[i-1])/"\n")[-1]/" " - ({ "" }));

      mention_issue(sha, issue_id);

      // Support syntax such as:
      // Fixes [PIKE-XXX].
      // Fixes remainder of PIKE-XXX.
      // Fix of issue PIKE-XXX.
      // etc...
      //
      // FIXME: Support "Fix PIKE-XXX and PIKE-YYY" or "Fix PIKE-XXX/PIKE-YYY"?
      foreach(({ "[", "issue", "bug", "of", "remainder" }), string word) {
	if (sizeof(b) && (b[-1] == word)) {
	  b = b[..sizeof(b)-2];
	}
      }
      if (sizeof(b) && has_prefix(b[-1], "fix")) {
        mention_issue(sha, issue_id, "fixed");
      }
      if (sizeof(b) && has_prefix(b[-1], "test")) {
        mention_issue(sha, issue_id, "tests");
      }
      if (sizeof(b) && has_prefix(b[-1], "doc")) {
        mention_issue(sha, issue_id, "documents");
      }
    }
  }
}

// API
int(0..1) check_commit(string branch, string sha, array(string) paths,
                       int|void just_check)
{
  if (::check_commit(branch, sha, paths)) {
    return 1;
  }

  mapping(int:multiset(string)) issue_mentions = commit_issues[sha];
  if (!issue_mentions) return 0;

  foreach(sort(indices(issue_mentions)), int issue_id) {
    multiset(string) mentions = issue_mentions[issue_id];
    mapping(string:mixed) issue = get_issue(issue_id);
    if (!issue) continue;
    string msg = update_issue_state(sha, issue, paths, mentions, branch);
    if (msg) {
      write("Issue workflow error: %s\n", msg);
      return 1;
    }
  }

  update_issues(just_check);

  return 0;
}

protected void create()
{
  // Allow for overrides via environment variables.
  gitlab_access_token = getenv("GITLAB_ACCESS_TOKEN");
  if (gitlab_access_token) {
    // Switch to direct access if we have an access token.
    gitlab_url = default_gitlab_url;
  } else {
    gitlab_url = default_gitlab_ro_url;
  }
  gitlab_url = getenv("GITLAB_URL") || gitlab_url;
  gitlab_project_id = ((int)getenv("GITLAB_PROJECT_ID")) ||
    default_gitlab_project_id;
}
