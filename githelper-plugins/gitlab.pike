//
// GitLab issue lookup.
//

// URL to the GitLab server (or proxy).
protected string gitlab_url;

// GitLab project identifier.
protected int gitlab_project_id;

// GitLab private access token.
// Optional, if not set only read-only operations to fetch issues
// will be performed. Note that this currently requires a proxy.
// If set it must provide the "api" privilege.
protected string gitlab_access_token;

protected Standards.URI gitlab_api_url(string path)
{
  return Standards.URI(sprintf("/api/v4/projects/%d/%s",
			       gitlab_project_id, path),
		       gitlab_url);
}

protected Protocols.HTTP.Query gitlab_con;

protected mixed gitlab_request(string method, string path,
			       mapping(string:string)|void variables,
			       string|void data)
{
  mapping(string:string) headers = ([]);

  if (gitlab_access_token) {
    headers["PRIVATE-TOKEN"] = gitlab_access_token;
  }

  mixed err = catch {
      gitlab_con =
	Protocols.HTTP.do_method(method, gitlab_api_url(path),
				 variables, headers, gitlab_con, data);
    };
  if (err) {
    // Paranoia; we do not want the token to show up in backtraces.
    headers["PRIVATE-TOKEN"] = "CENSORED";
    throw(err);
  }
  if (!gitlab_con() || (gitlab_con->status >= 300)) {
    write("NOTICE: GitLab request failed with code %d\n", gitlab_con->status);
    write("Data: %O\n", gitlab_con->unicode_data());
    gitlab_con = UNDEFINED;
    return UNDEFINED;
  }
  return Standards.JSON.decode(gitlab_con->unicode_data());
}

// The first element of the array is the current state, the second
// is the stored state.
protected mapping(int:array(mapping(string:mixed))) issues = ([]);

protected mapping(string:mixed) get_issue(int issue_id)
{
  array(mapping(string:mixed)) res_pair = issues[issue_id];
  if (!zero_type(res_pair)) return res_pair?res_pair[0]:UNDEFINED;

  mapping(string:mixed) res =
    gitlab_request("GET", sprintf("issues/%d", issue_id));
  if (res && mappingp(res)) {
    issues[issue_id] = ({ res, res + ([]) });
    return res;
  }

  write("NOTICE: No such issue: #%d\n", issue_id);
  issues[issue_id] = 0;
  return UNDEFINED;
}

protected void report_issue_state(int issue_id, int|void just_check)
{
}

protected void update_issue(int issue_id, int|void just_check)
{
  array(mapping(string:mixed)) issue_pair =
    just_check?issues[issue_id]:m_delete(issues, issue_id);
  if (!issue_pair) return;

  mapping(string:mixed) updated_fields = ([]);

  if (!equal(sort(issue_pair[0]->labels), sort(issue_pair[1]->labels))) {
    updated_fields->labels = issue_pair[0]->labels * ",";
  }

  if ((issue_pair[0]->state == "closed") &&
      (issue_pair[1]->state != "closed")) {
    updated_fields->state_event = "close";
  }

  write("Issue #%d: %s\n", issue_id, issue_pair[0]->title);

  if (!sizeof(updated_fields)) return;

  foreach(sort(issue_pair[1]->labels), string label) {
    if (has_value(issue_pair[0]->labels, label)) continue;
    write("  Removing label: %s\n", label);
  }
  foreach(sort(issue_pair[0]->labels), string label) {
    if (has_value(issue_pair[1]->labels, label)) continue;
    write("  Adding label: %s\n", label);
  }
  if (updated_fields->state_event == "close") {
    write("  Closing issue.\n");
  }
  report_issue_state(issue_id, just_check);

  if (just_check) return;

  gitlab_request("PUT", sprintf("issues/%d", issue_id), updated_fields);
}

protected void update_issues(int|void just_check)
{
  foreach(sort(indices(issues)), int issue_id) {
    update_issue(issue_id, just_check);
  }
}

protected void close_issue(int|mapping issue)
{
  if (intp(issue)) issue = get_issue(issue);
  issue->state = "closed";
}

protected void add_label(int|mapping issue, string label)
{
  if (intp(issue)) issue = get_issue(issue);
  if (!has_value(issue->labels, label)) {
    issue->labels += ({ label });
  }
}

protected void sub_label(int|mapping issue, string label)
{
  if (intp(issue)) issue = get_issue(issue);
  issue->labels -= ({ label });
}

protected mapping(string:mapping(int:multiset(string))) commit_issues = ([]);

protected void mention_issue(string sha, int issue_id, string|void mention)
{
  // Here for the side-effect of checking that the issue_id is valid.
  mapping(string:mixed) issue = get_issue(issue_id);

  mapping(int:multiset(string)) issue_mentions = commit_issues[sha] || ([]);
  commit_issues[sha] = issue_mentions;

  multiset(string) mentions = issue_mentions[issue_id] || (<>);
  issue_mentions[issue_id] = mentions;

  if (mention) {
    mentions[mention] = 1;
  }
}

// API
void check_message_body(string sha, array(string) headers, string body)
{
  array(string) a = body/"#";
  for (int i = 1; i < sizeof(a); i++) {
    int issue_id;
    if (sscanf(a[i], "%d", issue_id) && (issue_id > 0)) {
      array(string) b = ((lower_case(a[i-1])/"\n")[-1]/" " - ({ "" }));

      if (sizeof(b) && b[-1] == "rt") {
	// Reference to an RT issue. Skip.
	continue;
      }

      mention_issue(sha, issue_id);

      // Support syntax such as:
      // Fixes #XXX.
      // Fixes remainder of #XXX.
      // Fix of issue #XXX.
      // etc...
      //
      // FIXME: Support "Fix #XXX and #YYY" or "Fix #XXX/#YYY"?
      foreach(({ "issue", "bug", "of", "remainder" }), string word) {
	if (sizeof(b) && (b[-1] == word)) {
	  b = b[..sizeof(b)-2];
	}
      }
      if (sizeof(b) && has_prefix(b[-1], "fix")) {
	mention_issue(sha, issue_id, "fixed");
      }
    }
  }
}

// API
int(0..1) check_commit(string branch, string sha, array(string) paths,
                       int|void just_check)
{
  string name = replace(lower_case(basename(branch)),
			({ "_", "-" }), ({ "", "" }));
  int issue_id;
  foreach(({ "bug", "fix", "issue", "patch" }), string prefix) {
    if (has_prefix(name, prefix) && (issue_id = (int)name[sizeof(prefix)..])) {
      mention_issue(sha, issue_id);
    }
  }

  return 0;
}

protected void create()
{
  // Allow for overrides via environment variables.
  gitlab_access_token = getenv("GITLAB_ACCESS_TOKEN");
  gitlab_url = getenv("GITLAB_URL") || gitlab_url;
  gitlab_project_id = ((int)getenv("GITLAB_PROJECT_ID"));

  if (!gitlab_url || !gitlab_project_id) {
    write("NOTICE: GitLab plugin not configured.\n");
    destruct(this_object());
  }
}
